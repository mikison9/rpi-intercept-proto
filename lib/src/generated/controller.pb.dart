///
//  Generated code. Do not modify.
//  source: controller.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class Wifi extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Wifi', createEmptyInstance: create)
    ..aOS(1, 'ssid')
    ..aOS(2, 'password')
    ..hasRequiredFields = false
  ;

  Wifi._() : super();
  factory Wifi() => create();
  factory Wifi.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Wifi.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Wifi clone() => Wifi()..mergeFromMessage(this);
  Wifi copyWith(void Function(Wifi) updates) => super.copyWith((message) => updates(message as Wifi));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Wifi create() => Wifi._();
  Wifi createEmptyInstance() => create();
  static $pb.PbList<Wifi> createRepeated() => $pb.PbList<Wifi>();
  @$core.pragma('dart2js:noInline')
  static Wifi getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Wifi>(create);
  static Wifi _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get ssid => $_getSZ(0);
  @$pb.TagNumber(1)
  set ssid($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSsid() => $_has(0);
  @$pb.TagNumber(1)
  void clearSsid() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class Host extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Host', createEmptyInstance: create)
    ..aOS(1, 'mac')
    ..hasRequiredFields = false
  ;

  Host._() : super();
  factory Host() => create();
  factory Host.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Host.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Host clone() => Host()..mergeFromMessage(this);
  Host copyWith(void Function(Host) updates) => super.copyWith((message) => updates(message as Host));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Host create() => Host._();
  Host createEmptyInstance() => create();
  static $pb.PbList<Host> createRepeated() => $pb.PbList<Host>();
  @$core.pragma('dart2js:noInline')
  static Host getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Host>(create);
  static Host _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get mac => $_getSZ(0);
  @$pb.TagNumber(1)
  set mac($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMac() => $_has(0);
  @$pb.TagNumber(1)
  void clearMac() => clearField(1);
}

class Empty extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Empty', createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  Empty._() : super();
  factory Empty() => create();
  factory Empty.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Empty.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Empty clone() => Empty()..mergeFromMessage(this);
  Empty copyWith(void Function(Empty) updates) => super.copyWith((message) => updates(message as Empty));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Empty create() => Empty._();
  Empty createEmptyInstance() => create();
  static $pb.PbList<Empty> createRepeated() => $pb.PbList<Empty>();
  @$core.pragma('dart2js:noInline')
  static Empty getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Empty>(create);
  static Empty _defaultInstance;
}

