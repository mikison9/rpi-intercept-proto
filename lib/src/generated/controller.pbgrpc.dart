///
//  Generated code. Do not modify.
//  source: controller.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'controller.pb.dart' as $0;
export 'controller.pb.dart';

class ControllerClient extends $grpc.Client {
  static final _$connect = $grpc.ClientMethod<$0.Wifi, $0.Empty>(
      '/Controller/Connect',
      ($0.Wifi value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Empty.fromBuffer(value));
  static final _$startExploiting = $grpc.ClientMethod<$0.Empty, $0.Empty>(
      '/Controller/StartExploiting',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Empty.fromBuffer(value));
  static final _$getConnected = $grpc.ClientMethod<$0.Empty, $0.Host>(
      '/Controller/GetConnected',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Host.fromBuffer(value));
  static final _$startSslsplit = $grpc.ClientMethod<$0.Empty, $0.Empty>(
      '/Controller/StartSslsplit',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Empty.fromBuffer(value));
  static final _$stopSslsplit = $grpc.ClientMethod<$0.Empty, $0.Empty>(
      '/Controller/StopSslsplit',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Empty.fromBuffer(value));

  ControllerClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.Empty> connect($0.Wifi request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$connect, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Empty> startExploiting($0.Empty request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$startExploiting, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseStream<$0.Host> getConnected($0.Empty request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getConnected, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseStream(call);
  }

  $grpc.ResponseFuture<$0.Empty> startSslsplit($0.Empty request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$startSslsplit, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Empty> stopSslsplit($0.Empty request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$stopSslsplit, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class ControllerServiceBase extends $grpc.Service {
  $core.String get $name => 'Controller';

  ControllerServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Wifi, $0.Empty>(
        'Connect',
        connect_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Wifi.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $0.Empty>(
        'StartExploiting',
        startExploiting_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $0.Host>(
        'GetConnected',
        getConnected_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($0.Host value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $0.Empty>(
        'StartSslsplit',
        startSslsplit_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $0.Empty>(
        'StopSslsplit',
        stopSslsplit_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
  }

  $async.Future<$0.Empty> connect_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Wifi> request) async {
    return connect(call, await request);
  }

  $async.Future<$0.Empty> startExploiting_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return startExploiting(call, await request);
  }

  $async.Stream<$0.Host> getConnected_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async* {
    yield* getConnected(call, await request);
  }

  $async.Future<$0.Empty> startSslsplit_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return startSslsplit(call, await request);
  }

  $async.Future<$0.Empty> stopSslsplit_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return stopSslsplit(call, await request);
  }

  $async.Future<$0.Empty> connect($grpc.ServiceCall call, $0.Wifi request);
  $async.Future<$0.Empty> startExploiting(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Stream<$0.Host> getConnected($grpc.ServiceCall call, $0.Empty request);
  $async.Future<$0.Empty> startSslsplit(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$0.Empty> stopSslsplit(
      $grpc.ServiceCall call, $0.Empty request);
}
