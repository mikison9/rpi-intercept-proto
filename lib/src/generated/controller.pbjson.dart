///
//  Generated code. Do not modify.
//  source: controller.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const Wifi$json = const {
  '1': 'Wifi',
  '2': const [
    const {'1': 'ssid', '3': 1, '4': 1, '5': 9, '10': 'ssid'},
    const {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

const Host$json = const {
  '1': 'Host',
  '2': const [
    const {'1': 'mac', '3': 1, '4': 1, '5': 9, '10': 'mac'},
  ],
};

const Empty$json = const {
  '1': 'Empty',
};

